using UnityEngine;

public class myrongivens : MonoBehaviour
{
    public AudioClip clickSound, cameraSound;

    public static myrongivens USE;

    private AudioSource susanalambert;

    private void Awake()
    {
       
        if (USE == null)
        {
            USE = this;
            DontDestroyOnLoad(gameObject);

            susanalambert = transform.GetChild(0).GetComponent<AudioSource>();

            marisolbowling();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void marisolbowling()
    {
        
        AudioListener.volume = PlayerPrefs.GetInt("MusicSetting", 1);
    }

    public void winifredmarrero()
    {
        AudioListener.volume = AudioListener.volume == 1 ? 0 : 1;

        PlayerPrefs.SetInt("MusicSetting", (int)AudioListener.volume);
        PlayerPrefs.Save();
    }

    public void meredithpennington(AudioClip clip)
    {
        susanalambert.PlayOneShot(clip);
    }
}
