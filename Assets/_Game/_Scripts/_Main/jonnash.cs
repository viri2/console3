using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class jonnash : MonoBehaviour
{
    public Camera cameraObj;
    public derickquintana coloringMenu, paintingMenu;

    [System.Serializable]
    public class derickquintana
    {
        public GameObject angelwright;
        public Color color;
        public Image image;
        public Sprite bettybutcher;
        public Sprite belindagriffin;
    }

    void Awake()
    {
        Camera.main.aspect = 16 / 9f;
    }

    void Start()
    {
        OnMenuButtonClicked(false);
    }

    public void OnMenuButtonClicked(bool isPainting)
    {
        PlayerPrefs.SetInt("isPainting", isPainting ? 1 : 0);
        PlayerPrefs.Save();

        paintingMenu.angelwright.SetActive(isPainting);
        coloringMenu.angelwright.SetActive(!isPainting);

        cameraObj.backgroundColor = isPainting ? paintingMenu.color : coloringMenu.color;
        paintingMenu.image.sprite = isPainting ? paintingMenu.bettybutcher : paintingMenu.belindagriffin;
        coloringMenu.image.sprite = !isPainting ? coloringMenu.bettybutcher : coloringMenu.belindagriffin;
    }

    public void mablehill()
    {

    }

    public void lorrainebradford()
    {
        if (vicenteferrell.Instance.eileenhinkle)
        {
            SceneManager.LoadScene("gms");

        }
    }
}
