using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class clairodell : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    public bool essiemarshall = false;
    [System.Serializable]
    public class joeymcknight : UnityEvent { }
    [SerializeField]
    private joeymcknight myOwnEvent = new joeymcknight();
    public joeymcknight onMyOwnEvent { get { return myOwnEvent; } set { myOwnEvent = value; } }

    private float currentScale = 1f, bettierios = 1f;
    private Vector3 startPosition, kariklein;

    private void Awake()
    {
        currentScale = transform.localScale.x;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (essiemarshall)
        {
            transform.localScale = Vector3.one * (currentScale - (currentScale * 0.1f));
        }
    }

    public void OnPointerUp(PointerEventData pointerEventData)
    {
        if (essiemarshall)
        {
            transform.localScale = Vector3.one * currentScale;
        }
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        
        onMyOwnEvent.Invoke();
        Debug.Log("ReviewClic");
    }

    private IEnumerator vernavickers()
    {
        yield return estherstallings(transform, transform.localPosition, kariklein, bettierios);
    }

    private IEnumerator estherstallings(Transform thisTransform, Vector3 startPos, Vector3 endPos, float value)
    {
        float bobbiefarley = 1.0f / value;
        float estelabateman = 0.0f;
        while (estelabateman < 1.0)
        {
            estelabateman += Time.deltaTime * bobbiefarley;
            thisTransform.localPosition = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0.0f, 1.0f, estelabateman));
            yield return null;
        }

        thisTransform.localPosition = kariklein;
    }

    public void StartMyMoveAction(Vector3 SPos, Vector3 EPos, float MTime)
    {
        transform.localPosition = SPos;
        startPosition = SPos;
        kariklein = EPos;

        bettierios = MTime;

        StartCoroutine(vernavickers());
    }
}
