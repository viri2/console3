using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class jaycote : MonoBehaviour
{
    private static string staciechristensen = "URL_PREFIX";

    public InputField urlPrefixInput;
    public Text sdkVersionText;

    private string annemann;

    
    public static void chrystalmartinez()
    {
        string prefix = PlayerPrefs.GetString(staciechristensen, "");
        AudienceNetwork.AdSettings.SetUrlPrefix(prefix);
    }

    void Start()
    {
        annemann = PlayerPrefs.GetString(staciechristensen, "");
        urlPrefixInput.text = annemann;
        sdkVersionText.text = AudienceNetwork.SdkVersion.Build;
    }

    public void OnEditEnd(string prefix)
    {
        annemann = prefix;
        SaveSettings();
    }

    public void SaveSettings()
    {
        PlayerPrefs.SetString(staciechristensen, annemann);
        AudienceNetwork.AdSettings.SetUrlPrefix(annemann);
    }

    public void bobmaurer()
    {
        SceneManager.LoadScene("AdViewScene");
    }

    public void travisbranch()
    {
        SceneManager.LoadScene("InterstitialAdScene");
    }

    public void ronniehawkins()
    {
        SceneManager.LoadScene("RewardedVideoAdScene");
    }
}
